// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library decoder;

/*
 * This decoder takes a [Uint8List] of [bytes] and returns DICOM SOPInstance object or an
 * MSDICOM PatientStudy object.  It does the following:
 *   1. Checks to see if [bytes] has an MSDICOM header
 *      a. If so, it reads the header and then decrypts and decompresses the [bytes] as
 *         necessary.
 *   2. Checks to see if [bytes] has a DICOM PS3.10 Header
 *      a. If so, it reads the File Meta Information.
 *   3. Otherwise, check to see if it is a DICOM dataset (what is best way to do this?)
 */

