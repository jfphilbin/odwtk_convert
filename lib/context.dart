// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library convert.context;

//import 'package:dictionary/eval_mode.dart';
import 'package:io/io.dart';
import 'package:model/model.dart';

/**
 * Contains the [Dataset] being decoded, the [ReadBuffer] being read, the [limit] in the
 * [ReadBuffer] for contents of the current [Dataset], and the [EvalMode] ([LAZY, NORMAL, or
 * EAGER) of the decoder.
 *
 * Context is used for the recursive reading of Datasets, Sequences and Items.
 */

class Context {
        Context          previous; // The [Context] that created [this].
        TypedDataReader rb;
  final int           seqTag;   // The tag of the sequence containing this dataset or 0 if top level
  final int           limit;    // farthest length that can be read by this context
  final List<Dataset> items = new List();
  // is this needed?
        Dataset       ds;
  //TODO should this be at a higher level?
  final EvalMode      mode;

  Context(this.previous, this.seqTag, this.limit, [this.mode = EvalMode.EAGER]) {
    rb = previous.rb;
  }

  Context.topLevel(this.seqTag, this.limit, [this.mode = EvalMode.EAGER]) {
    previous = null;
    rb = previous.rb;
  }
}