DCMiD Project
Copyright 2014 Johns Hopkins University
Author: James F Philbin <james.philbin@jhmi.edu>

The Convert library contains tools implemented with the Dart Converter interface that can
convert byte streams containing a media type into a PatientStudies data structure.  It 
also contains tools that can convert the PatientStudies data structure into byte streams.

The bytes streams contain media types.  The following media types will eventually be 
supported:

* Binary DICOM in SFD format - this is the traditional dicom format

* Binary DICOM in MSD format - this is a new proposed media type

* JSON

* XML

TODO: finish this write up as the code is written and tested