// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library convert.msd_decoder;

/**
 * Reads a stream of bytes containing an MSDICOM encoded PatientStudy and converts
 * them into a Patient Study object.
 *
 * MSDICOM studies have the following characteristics:
 *   1. They are encoded with Explicit Little Endian
 *   2. The transfer syntax is specified using the tag (0006,9908) for now.
 *   3  The character set is UTF-8 by default, but can be change using the ???
 *      tag (0008,0005)
 *   See <msdicom_format.md> for more details.
 */

//TODO add a MediaType tag (0x00069908) for now.
//TODO remove FMI from everywhere but PatientStudies and add MediaType tag instead
//     it should be added to every image object in Bulkdata

import 'dart:typed_data';

import 'package:convert/context.dart';
import 'package:core/core.dart';
import 'package:io/read_buffer.dart';
import 'package:model/model.dart';


//import 'package:utilities/utilities.dart';

/**
 * Read MSD binary data from a ByteBuf
 */
class MSDDecoder {
  //TODO should these fields be private or final?
  String     fileUri;
  EvalMode   evalMode;
  ReadBuffer read;
  Context    ctx;

  MSDDecoder(Uint8List bytes, [String this.fileUri = "",
                               EvalMode this.evalMode = EvalMode.EAGER]) {
  }

  MSDDecoder.fromFile(String filename, [evalMode = EvalMode.EAGER])

  PatientStudies decode(Uint8List bytes) {
    ReadBuffer reader = new ReadBuffer(bytes);
    MintHeader mintHdr;
    Context    ctx;

    //TODO define preamble
    mintHdr = maybeReadMintHeader();
    Dataset fmi = maybeReadFMI();
    Preamble preamble = readPreamble(reader);
    int tag = reader.peek8();
    if (tag != PatientStudiesSequence) {
      parseError("This is not an MSDICOM media type");
    }
    ctx  = new Context(0, new Dataset(), reader.writeIndex,
                       [this.mode = EvalMode.EAGER]);
    return readPatientStudies(reader, ctx);
  }
}

MintHeader maybeReadMintHeader(ByteBuf bb) {

}

FileMeta maybeReadPart10Header(FileMeta fmi) {
  int tag = peekTag();
  if (tag == 0) {
    // Read Part 10 format
    bb.skip(128);
    String dicm = bb.readString(maxLength: 4);
    if (dicm == "DICM") {
      print('PS3.10 file');
      return readFMI(fmi);
    } else {
      parseError('bad PS3.10 header');
    }
  } else if (tag > 0x00020000 && tag < 0x00020102) {
    print('Bad FMI format');
    return readFMI(fmi);
  } else if (tag > 0x00080000 && tag < 0x00080021) {
    print('No FMI in Metadata');
    return null;
  } else {
    parseError("Bad Metadata Format: $this");
  }
}





  PatientStudies readPatientStudy(FileMeta fmi, Uri fileUri, EvalMode evalMode) {
    ps = new PatientStudies(fmi, fileUri);
    stack = new DSStack();
    stack.push(ps);

  }


  PatientStudies readPatientStudies(int nBytes) {
    int limit = bb.readLimit(nBytes);
    Dataset fmi = readItem();
    Dataset patient = readItem();
    //TODO when and how to get UIDs
    //UID uid = ???;
    PatientStudies ps = new PatientStudies(fmi, patient);
    while (bb.notAtReadLimit(limit)) ps.studies.add(readItem());
    return ps;
  }

  FileMeta readFMI(FileMeta fmi) {
    if (peekTag() < 0x00020000) parseError('error trying to read FMI');
    while (peekTag() < 0x00020103) {
      readAttribute(fmi);
    }
    return fmi;
  }

  List<Study> readStudies(int limit) {
    while(bb.readIndex < limit) {
      Study study = readStudy(limit);
    }
  }

  Study readStudy(int limit) {
    Study study = new Study();
    study.attrs = readItem();
    study.initialize();
    return study;
  }

  Study readStudy(int nBytes) {
    int limit = bb.readLimit(nBytes);
    Dataset fmi = readItem();
    Dataset dataset = readItem();
    UID uid = dataset.getStudyUid();
    return new Study(fmi, dataset, uid);
  }


  Study readSeries(int limit) {
    Study series = new Series();
    series.attrs = readItem();
    series.initialize();
    return series;
  }

  Series readSeries(int nBytes) {
    Series series = new Series();
  }


  Study readInstance(int limit) {
    Study instance = new Instance();
    instance.attrs = readItem();
    instance.initialize();
    return instance;
  }

  Instance readInstance(int nBytes) {
    Instance instance = new Instance();
  }

// end of class
}
