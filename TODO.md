Convert Package

ToDo:
  1. create readDataset with Context and use it to read sfd and msd files.  It should
     recursively read the entire dataset in the file.
  2. create readSFD (explicitLE) that reads a single sfd file and returns an sfd object
  3. create readMSD (explicitLE) that reads a MSD metadata file and returns a study.
  4. create readSFDDirectory that reads all sfd files in a directory.
  5. create SFDToMSD
  