// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>

/// File used for quickly generating random tests or output

//import 'dart:convert';
import 'dart:typed_data';
//import 'dart:math';

//import 'package:convert/bytebuf.dart';
//import 'package:utilities/utilities.dart';
//import 'package:core/core.dart';

class TestData {
  String string;
  VR vr;
  int length;
  ByteData bd;
  ByteBuf bb;

  TestData(this.string, this.vr) {
    length = string.length;
    bd = new ByteData.view(toUint8.buffer);
    bb = new ByteBuf.fromByteData(bd);
  }

  int       get maxLength => min(vr.maxLength, length);
  Uint8List get toUint8 => new Uint8List.fromList(string.codeUnits);
  ByteBuf   get toByteBuf => new ByteBuf.fromByteData(bd);
  int       get toInt     => int.parse(string);
  double    get toFloat   => double.parse(string);

  List get toList {
    List list = new List();
    int start = 0;
    for (var i = 0; i < length; i++) {
      if (string[i] == "\\") {
        list.add(string.substring(start, i));
        i++;
        start = i;
      }
    }
    if (start != length) list.add(string.substring(start, length));
    return list;
  }

  toString() => 'TestData[length=$length, maxLength=${vr.maxLength}, string= $string]';

}

final dcmjson = [{"123": {"vr" : 1,
                    "value": [6]}},
           {"456": {"vr" : 2,
                    "value": [16]}},
           ];

void main() {

  List ul = 'MINT'.codeUnits;
  Uint8List l = new Uint8List.fromList(ul);
  ByteData bd = l.buffer.asByteData();
  int mint = bd.getUint32(0);
  print('mint=$mint');

  print('MINT=${'MINT'.codeUnits}');
/*
  print(dcmjson["123"]);
  print(dcmjson["456"]["vr"]);

  String vrCodeToString(int vrCode) {
    int b1 = vrCode & 255;
    int b2 = (vrCode >> 8) & 255;
    List list = [b1, b2];
    return ASCII.decode(list);
  }
*/

  print('VR=${vrCodeToString(19541)}, code=${intToHex(19541)}');
  print('${"UL".codeUnits}');

  /*
  List readStringList(TestData td) {
    ByteBuf bb = td.toByteBuf;
    int maxlen = td.vr.maxLength;
    print(bb.readStringList(td.length, maxItemLength: td.maxLength));
  }

  TestData DA1 = new TestData("19500718\\19520102\\19931118\\19960131", VR.DA); // 16 chars
  print(VR.DA);
  print(DA1.string);
  print(DA1.toList);
  readStringList(DA1);
  */

/*
  print(#foo);
  print(Ascii.UPPERCASE);
  const NULL = 0;
  const BACKSPACE = 8;
  const TAB = 9;
  const LINEFEED = 10;
  const NEWLINE = 10;
  const VIRTICAL_TAB = 11;
  const FORMFEED = 12;
  const RETURN = 13;
  const CARRIAGE_RETURN = 13;
  const ESCAPE = 27;
  const SPACE = 32;
  const SINGLE_QUOTE = 39;
  const DOUBLE_QUOTE = 34;
  const PLUS = 43;
  const MINUS = 45;
  const PERIOD = 46;
  const DIGIT_0 = 48;
  const DIGIT_9 = 57;
  const ATSIGN = 64;
  const LETTER_A = 65;
  const LETTER_E = 69;
  const LETTER_Z = 90;
  const BACKSLASH = 92;
  const UNDERSCORE = 95;
  const LETTER_a = 97;
  const LETTER_e = 101;
  const LETTER_z = 122;
  const TILDE = 126;


  BitList createAndSetBitList(int start, int end, [bool value = true]) {
    var list = new BitList(128);
    list.fillRange(start, end, value);
    return list;
  }

  BitList createAndSetRange(int start, int end, [bool value = true]) {
    var list = new BitList(128);
    list.fillRange(start, end, value);
    return list;
  }

  Uint32List blToUint32List(BitList bl) {
    return new Uint32List.fromList(bl.buffer);
  }


  BitList copyAndSet(BitList charset, List<int> chars) {
    List list = charset.clone();
    for (var char in chars) {
      list[char] = true;
    }
    if (charset == list) throw new Error();
    return list;
  }

  BitList copyAndClear(BitList charset, List<int> chars) {
    List list = charset.clone();
    for (var char in chars) {
      list[char] = false;
    }
    return list;
  }

  String generateBitFieldDefs(List<String, BitList> characterSets) {
    String output ="";
    for(var charset in characterSets) {
     // print(charset);
      String name = charset[0];
      //BitList data = charset[1];
      //output += "final ${name}_buffer = new Uint32List.fromList(${data.buffer});\n";
      Uint32List data = new Uint32List.fromList(charset[1].buffer);
      output += "const $name = const Ascii($name, const $data);\n";
      //output += "const ${charset[0]} = const ${charset[1]};\n";
    }
    print(output);
    return output;
  }

  final CONTROL = [LINEFEED, FORMFEED, RETURN, ESCAPE];
  final NO_ESCAPE = [LINEFEED, FORMFEED, RETURN];
  final DWMY_CHARSET = "DWMY".codeUnits;

  BitList uppercase  = createAndSetRange(LETTER_A, LETTER_Z + 1);
  BitList lowercase  = createAndSetRange(LETTER_a, LETTER_z + 1);
  BitList dcr        = createAndSetBitList(SPACE, TILDE + 1);
  BitList digits        = createAndSetRange(DIGIT_0, DIGIT_9 + 1);

  BitList aetitle    = copyAndClear(dcr, [BACKSLASH]);
  BitList age        = copyAndSet(digits, DWMY_CHARSET);
  BitList codeString = uppercase | digits;
  BitList date       = digits;
  BitList dateTime   = copyAndSet(digits, [PLUS, MINUS, PERIOD, SPACE]);
  BitList decimal    = copyAndSet(digits, [PLUS, MINUS, PERIOD, LETTER_E, LETTER_e]);
  BitList integer    = copyAndSet(digits, [PLUS, MINUS]);
  BitList personName = copyAndSet(aetitle, [ESCAPE]);
  BitList string     = copyAndSet(aetitle, [ESCAPE]);
  BitList time       = copyAndSet(digits, [PERIOD, SPACE]);
  BitList text       = copyAndSet(aetitle, CONTROL);
  BitList ui = copyAndSet(digits, [PERIOD]);
  BitList uri        = copyAndSet(aetitle, [BACKSLASH]);


  final List characterSets
      = [["UPPERCASE",    uppercase],
         ["LOWERCASE",    lowercase],
        // ["DCR",          dcr],
         ["DIGITS",       digits],
         ["AETITLE",      aetitle],
         ["AGE",          age],
         ["CODE_STRING",  codeString],
         ["DATE",         date],
         ["DATE_TIME",    dateTime],
         ["DECIMAL",      decimal],
         ["INTEGER",      integer],
         ["PERSON_NAME",  personName],
         ["STRING",       string],
         ["TIME",         time],
         ["TEXT",         text],
         ["UI",           ui],
         ["URI",          uri]];


  generateBitFieldDefs(characterSets);
  print(digits);
  */
}