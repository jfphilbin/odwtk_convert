// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>

//DCMiD Toolkit packages
import 'package:unittest/unittest.dart';
import 'package:io/bytebuf.dart';
import 'package:dictionary/vr.dart';

// Test Utilities
//import 'package:core/test/utilities_test.dart';
import 'test_data.dart';

void main() {

  //Helper functions
  String readString(TestData td) {
    ByteBuf bb = td.toByteBuf;
    return bb.readString(maxLength:td.readLength);
  }

  List readStringList(TestData td) {
    ByteBuf bb = td.toByteBuf;
    return bb.readStringList(td.length, maxItemLength: td.readLength);
  }

  //AETitle = VR.AE
  //VFSize = 2, MaxLength = 16, Multiple Values = Y
  TestData AE0 = new TestData("ab1", VR.AE);
  TestData AE1 = new TestData("0123456789012345", VR.AE); // 16 chars
  TestData AE2 = new TestData("", VR.AE);
  //TODO
  //Fail
  test('Read VR.AE', () {
    print(VR.AE);
    expect(readStringList(AE0), equals(AE0.toList));
    expect(readStringList(AE1), equals(AE1.toList));
    expect(readStringList(AE2), equals(AE2.toList));
    //TODO Fail
  });

  //Age = VR.AS
  //VFSize = 2, MaxLength = 4, Multiple Values = N
  TestData AS0 = new TestData("012D", VR.AS);
  TestData AS1 = new TestData("123W", VR.AS);
  TestData AS2 = new TestData("765M", VR.AS);
  TestData AS3 = new TestData("987Y", VR.AS);
  test('Read VR.AS', () {
    print(VR.AS);
    expect(readString(AS0), equals(AS0.string));
    expect(readString(AS1), equals(AS1.string));
    expect(readString(AS2), equals(AS2.string));
    expect(readString(AS3), equals(AS3.string));
  });

  //Code String = VR.CS
  //VFSize = 2, MaxLength = 16, Multiple Values = Y
  TestData CS0 = new TestData("ab1", VR.CS);
  TestData CS1 = new TestData("01234567\\89012345", VR.CS); // 16 chars
  test('Read VR.CS', () {
    print(VR.CS);
    expect(readStringList(CS0), equals(CS0.toList));
    expect(readStringList(CS1), equals(CS1.toList));
  });

  //Date = VR.DA
  //VFSize = 2, MaxLength = 8, Multiple Values = Y
  TestData DA0 = new TestData("20140506", VR.DA);
  TestData DA1 = new TestData("19500718\\19520102\\19931118\\19960131", VR.DA); // 16 chars
  test('Read VR.DA', () {
    print(VR.DA);
    expect(readStringList(DA0), equals(DA0.toList));
    expect(readStringList(DA1), equals(DA1.toList));
  });

  //Digital String = VR.DS
  //VFSize = 2, MaxLength = 16, Multiple Values = Y
  TestData DS0 = new TestData("0.0", VR.DT);
  TestData DS1 = new TestData("1.1", VR.DS);
  TestData DS2 = new TestData("01234567.8901234", VR.DS); // 16 chars
  TestData DS3 = new TestData(".12345", VR.DS);
  TestData DS4 = new TestData("01234567.8901234", VR.DS); // 16 chars
  TestData DS5 = new TestData("-1.1", VR.DS);
  TestData DS6 = new TestData("+1234567.8901234", VR.DS); // 16 chars
  TestData DS7 = new TestData(".12345E10", VR.DS);
  TestData DS8 = new TestData("01234567.901e345", VR.DS); // 16 chars
  // Fail
  TestData NDS0 = new TestData("", VR.DS); // 0 chars
  TestData NDS1 = new TestData("\\", VR.DS); // 0 chars
  TestData NDS2 = new TestData("01234567.89012345", VR.DS); // 17 chars
  TestData NDS3 = new TestData("+/-", VR.DS); // 0 chars
  TestData NDS4 = new TestData("01234567.89A12345", VR.DS); // 17 chars
  test('Read VR.DS', () {
    print(VR.DS);
    expect(readStringList(DS0), equals(DS0.toList));
    expect(readStringList(DS1), equals(DS1.toList));
    expect(readStringList(DS2), equals(DS2.toList));
    expect(readStringList(DS3), equals(DS3.toList));
    expect(readStringList(DS4), equals(DS4.toList));
    expect(readStringList(DS5), equals(DS5.toList));
    expect(readStringList(DS6), equals(DS6.toList));
    expect(readStringList(DS7), equals(DS7.toList));
    expect(readStringList(DS8), equals(DS8.toList));
    //Fail
    expect(readStringList(NDS0), equals(NDS0.toList));
    expect(readStringList(NDS1), equals(NDS1.toList));
    expect(readStringList(NDS2), equals(NDS2.toList));
    expect(readStringList(NDS3), equals(NDS3.toList));
  });

  //Age = VR.DT
  //VFSize = 2, MaxLength = 26, Multiple Values = Y
  TestData DT0 = new TestData("19501231235959", VR.AS);   // No ms or tz
  TestData DT1 = new TestData("20140505000000.123456", VR.AS); //full
  TestData DT2 = new TestData("20130405010203.123456+0130", VR.AS);
  TestData DT3 = new TestData("19500505003030.123-0245", VR.AS);
  //Fail
  TestData NDT0 = new TestData("19501231235959", VR.AS);   // No ms or tz
  TestData NDT1 = new TestData("20140505000000.123456", VR.AS); //full
  TestData NDT2 = new TestData("20130405010203.123456+0130", VR.AS);
  TestData NDT3 = new TestData("19500505003030.123-0245", VR.AS);
  //TODO
  test('Read VR.DT', () {
    print(VR.DT);
    expect(readStringList(DT0), equals(DT0.toList));
    expect(readStringList(DT1), equals(DT1.toList));
    expect(readStringList(DT2), equals(DT2.toList));
    expect(readStringList(DT3), equals(DT3.toList));
    //Fail
    expect(readStringList(NDT0), equals(NDT0.toList));
    expect(readStringList(NDT1), equals(NDT1.toList));
    expect(readStringList(NDT2), equals(NDT2.toList));
    expect(readStringList(NDT3), equals(NDT3.toList));
  });

  /*
  //Code String = VR.IS
  //VFSize = 2, MaxLength = 16, Multiple Values = Y
  TestData aSingleCodeString = new TestData("ab1", VR.CS);
  TestData aCoupleOfCodeStrings = new TestData("01234567\\89012345", VR.CS); // 16 chars
  test('Read VR.CS', () {
    print(VR.CS);
    expect(readStringList(aSingleCodeString), equals(aSingleCodeString.toList));
    expect(readStringList(aCoupleOfCodeStrings), equals(aCoupleOfCodeStrings.toList));
  });

  //Date = VR.LO
  //VFSize = 2, MaxLength = 8, Multiple Values = Y
  TestData aDate = new TestData("20140506", VR.DA);
  TestData someDates = new TestData("19500718\\19520102\\19931118\\19960131", VR.DA); // 16 chars
  test('Read VR.DA', () {
    print(VR.DA);
    expect(readStringList(aDate), equals(aDate.toList));
    expect(readStringList(someDates), equals(someDates.toList));
  });

  //AETitle = VR.LT
  //VFSize = 2, MaxLength = 16, Multiple Values = Y
  TestData aSmallAETitle = new TestData("ab1", VR.LT);
  TestData aBigAETitle = new TestData("0123456789012345", VR.LT); // 16 chars
  TestData aNonAETitle = new TestData("", VR.LT); // 16 chars
  test('Read VR.LT', () {
    print(VR.LT);
    expect(readStringList(aSmallAETitle), equals(aSmallAETitle.toList));
    expect(readStringList(aBigAETitle), equals(aBigAETitle.toList));
    expect(readStringList(aNonAETitle), equals(aNonAETitle.toList));
  });

  //Age = VR.PN
  //VFSize = 2, MaxLength = 4, Multiple Values = N
  TestData aVeryYoungAge = new TestData("012D", VR.AS);
  TestData aYoungAge = new TestData("123W", VR.AS);
  TestData anOlderAge = new TestData("765M", VR.AS);
  TestData aVeryOldAge = new TestData("987Y", VR.AS);
  solo_test('Read VR.AS', () {
    print(VR.AS);
    expect(readStringList(aVeryYoungAge), equals(aVeryYoungAge.toList));
    expect(readStringList(aYoungAge), equals(aYoungAge.toList));
    expect(readStringList(anOlderAge), equals(anOlderAge.toList));
    expect(readStringList(aVeryOldAge), equals(aVeryOldAge.toList));
  });

  //Code String = VR.SH
  //VFSize = 2, MaxLength = 16, Multiple Values = Y
  TestData aSingleCodeString = new TestData("ab1", VR.CS);
  TestData aCoupleOfCodeStrings = new TestData("01234567\\89012345", VR.CS); // 16 chars
  test('Read VR.CS', () {
    print(VR.CS);
    expect(readStringList(aSingleCodeString), equals(aSingleCodeString.toList));
    expect(readStringList(aCoupleOfCodeStrings), equals(aCoupleOfCodeStrings.toList));
  });

  //Date = VR.ST
  //VFSize = 2, MaxLength = 8, Multiple Values = Y
  TestData aDate = new TestData("20140506", VR.DA);
  TestData someDates = new TestData("19500718\\19520102\\19931118\\19960131", VR.DA); // 16 chars
  test('Read VR.DA', () {
    print(VR.DA);
    expect(readStringList(aDate), equals(aDate.toList));
    expect(readStringList(someDates), equals(someDates.toList));
  });

  //AETitle = VR.TM
  //VFSize = 2, MaxLength = 16, Multiple Values = Y
  TestData aSmallAETitle = new TestData("ab1", VR.TM);
  TestData aBigAETitle = new TestData("0123456789012345", VR.TM); // 16 chars
  TestData aNonAETitle = new TestData("", VR.TM); // 16 chars
  test('Read VR.TM', () {
    print(VR.TM);
    expect(readStringList(aSmallAETitle), equals(aSmallAETitle.toList));
    expect(readStringList(aBigAETitle), equals(aBigAETitle.toList));
    expect(readStringList(aNonAETitle), equals(aNonAETitle.toList));
  });

  //Age = VR.UI
  //VFSize = 2, MaxLength = 4, Multiple Values = N
  TestData aVeryYoungAge = new TestData("012D", VR.AS);
  TestData aYoungAge = new TestData("123W", VR.AS);
  TestData anOlderAge = new TestData("765M", VR.AS);
  TestData aVeryOldAge = new TestData("987Y", VR.AS);
  solo_test('Read VR.AS', () {
    print(VR.AS);
    expect(readStringList(aVeryYoungAge), equals(aVeryYoungAge.toList));
    expect(readStringList(aYoungAge), equals(aYoungAge.toList));
    expect(readStringList(anOlderAge), equals(anOlderAge.toList));
    expect(readStringList(aVeryOldAge), equals(aVeryOldAge.toList));
  });

  //Code String = VR.UR
  //VFSize = 2, MaxLength = 16, Multiple Values = Y
  TestData aSingleCodeString = new TestData("ab1", VR.CS);
  TestData aCoupleOfCodeStrings = new TestData("01234567\\89012345", VR.CS); // 16 chars
  test('Read VR.CS', () {
    print(VR.CS);
    expect(readStringList(aSingleCodeString), equals(aSingleCodeString.toList));
    expect(readStringList(aCoupleOfCodeStrings), equals(aCoupleOfCodeStrings.toList));
  });

  //Date = VR.UT
  //VFSize = 4, MaxLength = 10, Multiple Values = N
  TestData aDate = new TestData("20140506", VR.DA);
  TestData someDates = new TestData("19500718\\19520102\\19931118\\19960131", VR.DA); // 16 chars
  test('Read VR.DA', () {
    print(VR.DA);
    expect(readStringList(aDate), equals(aDate.toList));
    expect(readStringList(someDates), equals(someDates.toList));
  });
  */

}  // End of Main()


