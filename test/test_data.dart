// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library test_data;
//import 'dart:convert';
import 'dart:math';
import 'dart:typed_data';

//Imaging 3.0 Core Toolkit packages
import 'package:io/bytebuf.dart';
import 'package:dictionary/vr.dart';

class TestData {
  String string;
  VR vr;
  int length;
  ByteData bd;
  ByteBuf bb;

  TestData(this.string, this.vr) {
    length = string.length;
    bd = new ByteData.view(toUint8.buffer);
    bb = new ByteBuf.fromByteData(bd);
  }

  int       get readLength => min(vr.maxLength, length);
  Uint8List get toUint8 => new Uint8List.fromList(string.codeUnits);
  ByteBuf   get toByteBuf => new ByteBuf.fromByteData(bd);
  int       get toInt     => int.parse(string);
  double    get toFloat   => double.parse(string);

  List get toList {
    List list = new List();
    int start = 0;
    for (var i = 0; i < length; i++) {
      if (string[i] == "\\") {
        list.add(string.substring(start, i));
        i++;
        start = i;
      }
    }
    if (start != length) {
      list.add(string.substring(start, length));
    }
    return list;
  }

  String readString() {
    ByteBuf bb = toByteBuf;
    return bb.readString(maxLength: readLength);
  }

  List readStringList() {
    ByteBuf bb = toByteBuf;
    return bb.readStringList(length, maxItemLength: readLength);
  }

  toString() => 'TestData[length=$length, maxLength=${vr.maxLength}, string= $string]';

}