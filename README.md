## DWDK<sup>*</sup> Convert

This package contains **converter** classes for encoding and decoding the DICOM information model into different media types.  Both the  traditional DICOM format and the Multi-Study DICOM format (**MSD**) will be supported.  The convert package will eventually support binary, JSON, and XML media types. The package also include transformers that will read an object using one media type and write it using a different media type.


**\*DICOMweb Toolkit**