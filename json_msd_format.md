#Multi-Study DICOM JSON Media Type (msdicom+json)

This document describes both the DICOM JSON Media Type and The MSDICOM JSON
Media Type.  While the two media types are similar, there are some important
differences:

MSDICOM can:
* Encode multiple studies for a single patient in one object;

* Encode any part of a study, from one frame to any number or type of 
Information Entity in a single object;

* Encodes the Media Type of the objects contained in a study;

* Separate the contained Studies into Metadatra and Bulkdata objects.

## DICOM+JSON Media Type
The `dicom+json` media type is encoded as follows: conventions and 
representations as the `dicom+json` media type:

* A **Dataset** is represented as an array of attributes

* An **Attribute** is represented by an attribute object containing 
  *one* **name/value pair**. 
 
  **name**: is a string containing the 8 character uppercase hexadecimal
     representation of a DICOM tag.

	**value**: is a JSON object containing *two* name/value pairs.

  1. The first pair has the **name** "vr" and a **value** that is a two 
		character string that corresponds to the name of the *Value
        Representation* of the Attribute.
    
  2. The second pair has the **name** "value" and a **value** that is an 
	    array containing zero or more *values* of the Attrbute.
       
The following is an example DICOM JSON *Dataset*:

	[  
     {"0008000D": {  
    	"vr": "UI",  
        "value": [ "1.2.392.200036.9116.2.2.2.1762893313.1029997326.945873" ]  
    	}
     }
     {"00100010" : {  
        "vr": "UI",
        "value": [ "1.2.392.200036.9116.2.2.2.2162893313.1029997326.945876" ]
     	}
   	 }

	...

	 {"0020000D" : {
        "vr": "UI",
        "value": [ "1.2.392.200036.9116.2.2.2.2162893313.1029997326.945876" ]
     	}
   	 }
	]

However, a better encoding might be:

	[[0x00100010, "UI", 98765]
	 [0x00100012, "DA", "20140604"]
	 [0x00100014, "TM", "124258"]
	  ...
	 [0x0FFFFFFE, "OB", b64"....."]
	]

The JSON encoding of DICOM data types:

**Dataset**
: An array of attributes

**Attribute**
: Either a **Simple-Attribute** or a **Sequence**

**Simple-Attribute**
: An array with three elements [**tag**, **vr**, **values**]

**Sequence**
: An array of zero or more **Datasets**

The following table shows encoding for the different Value Representations.
 
<table rules="all" frame="box"><thead><tr valign="top">
      <th align="center">
        <p><span class="bold"><strong>VR Name</strong></span></p>
      </th>
      <th align="center">
        <p><span class="bold"><strong>Type</strong></span></p>
      </th>
      <th align="center">
        <p><span class="bold"><strong>JSON Data Type</strong></span></p>
      </th>
    </tr></thead>
    <tbody>
    <tr valign="top">
              <td align="center">
                <p>AE</p>
              </td>
              <td align="left">
                <p>Application Entity</p>
              </td>
              <td align="left">
                <p>
                  <span class="italic">String</span>
                </p>
              </td>
            </tr><tr valign="top">
              <td align="center">
                <p>AS</p>
              </td>
              <td align="left">
                <p>Age String</p>
              </td>
              <td align="left">
                <p>
                  <span class="italic">String</span>
                </p>
              </td>
            </tr><tr valign="top">
              <td align="center">
                <p>AT</p>
              </td>
              <td align="left">
                <p>Attribute Tag</p>
              </td>
              <td align="left">
                <p>
                  <span class="italic">String</span>
                </p>
              </td>
            </tr><tr valign="top">
              <td align="center">
                <p>CS</p>
              </td>
              <td align="left">
                <p>Code String</p>
              </td>
              <td align="left">
                <p>
                  <span class="italic">String</span>
                </p>
              </td>
            </tr><tr valign="top">
              <td align="center">
                <p>DA</p>
              </td>
              <td align="left">
                <p>Date</p>
              </td>
              <td align="left">
                <p>
                  <span class="italic">String</span>
                </p>
              </td>
            </tr><tr valign="top">
              <td align="center">
                <p>DS</p>
              </td>
              <td align="left">
                <p>Decimal</p>
              </td>
              <td align="left">
                <p>
                  <span class="italic">Number</span>
                </p>
              </td>
            </tr><tr valign="top">
              <td align="center">
                <p>DT</p>
              </td>
              <td align="left">
                <p>Date Time</p>
              </td>
              <td align="left">
                <p>
                  <span class="italic">String</span>
                </p>
              </td>
            </tr><tr valign="top">
              <td align="center">
                <p>FL</p>
              </td>
              <td align="left">
                <p>Floating Point Single</p>
              </td>
              <td align="left">
                <p>
                  <span class="italic">Number</span>
                </p>
              </td>
            </tr><tr valign="top">
              <td align="center">
                <p>FD</p>
              </td>
              <td align="left">
                <p>Floating Point Double</p>
              </td>
              <td align="left">
                <p>
                  <span class="italic">Number</span>
                </p>
              </td>
            </tr><tr valign="top">
              <td align="center">
                <p>IS</p>
              </td>
              <td align="left">
                <p>Integer String</p>
              </td>
              <td align="left">
                <p>
                  <span class="italic">Number</span>
                </p>
              </td>
            </tr><tr valign="top">
              <td align="center">
                <p>LO</p>
              </td>
              <td align="left">
                <p>Long String</p>
              </td>
              <td align="left">
                <p>
                  <span class="italic">String</span>
                </p>
              </td>
            </tr><tr valign="top">
              <td align="center">
                <p>LT</p>
              </td>
              <td align="left">
                <p>Long Text</p>
              </td>
              <td align="left">
                <p>
                  <span class="italic">String</span>
                </p>
              </td>
            </tr><tr valign="top">
              <td align="center">
                <p>OB</p>
              </td>
              <td align="left">
                <p>Other Byte String</p>
              </td>
              <td align="left">
                <p><span class="italic">Base64</span> encoded
        <span class="italic">string</span></p>
              </td>
            </tr><tr valign="top">
              <td align="center">
                <p>OD</p>
              </td>
              <td align="left">
                <p>Other Double String</p>
              </td>
              <td align="left">
                <p><span class="italic">Base64</span> encoded
        <span class="italic">string</span></p>
              </td>
            </tr><tr valign="top">
              <td align="center">
                <p>OF</p>
              </td>
              <td align="left">
                <p>Other Float String</p>
              </td>
              <td align="left">
                <p><span class="italic">Base64</span> encoded
        <span class="italic">string</span></p>
              </td>
            </tr><tr valign="top">
              <td align="center">
                <p>OW</p>
              </td>
              <td align="left">
                <p>Other Word String</p>
              </td>
              <td align="left">
                <p><span class="italic">Base64</span> encoded
        <span class="italic">string</span></p>
              </td>
            </tr><tr valign="top">
              <td align="center">
                <p>PN</p>
              </td>
              <td align="left">
                <p>Person Name</p>
              </td>
              <td align="left">
                <p><span class="italic">Object</span> containing Person Name component groups as
        <span class="italic">strings (see <a class="xref" href="#sect_F.2.2" title="F.2.2 DICOM JSON Model Object Structure">Section F.2.2</a>)</span></p>
              </td>
            </tr><tr valign="top">
              <td align="center">
                <p>SH</p>
              </td>
              <td align="left">
                <p>Short String</p>
              </td>
              <td align="left">
                <p>
                  <span class="italic">String</span>
                </p>
              </td>
            </tr><tr valign="top">
              <td align="center">
                <p>SL</p>
              </td>
              <td align="left">
                <p>Signed Long</p>
              </td>
              <td align="left">
                <p>
                  <span class="italic">Number</span>
                </p>
              </td>
            </tr><tr valign="top">
              <td align="center">
                <p>SQ</p>
              </td>
              <td align="left">
                <p>Sequence</p>
              </td>
              <td align="left">
                <p><span class="italic">Array</span> of DICOM datasets, which 
                   are arrays of attributes</p>
              </td>
            </tr><tr valign="top">
              <td align="center">
                <p>SS</p>
              </td>
              <td align="left">
                <p>Signed Short</p>
              </td>
              <td align="left">
                <p>
                  <span class="italic">Number</span>
                </p>
              </td>
            </tr><tr valign="top">
              <td align="center">
                <p>ST</p>
              </td>
              <td align="left">
                <p>Short Text</p>
              </td>
              <td align="left">
                <p>
                  <span class="italic">String</span>
                </p>
              </td>
            </tr><tr valign="top">
              <td align="center">
                <p>TM</p>
              </td>
              <td align="left">
                <p>Time</p>
              </td>
              <td align="left">
                <p>
                  <span class="italic">String</span>
                </p>
              </td>
            </tr><tr valign="top">
              <td align="center">
                <p>UI</p>
              </td>
              <td align="left">
                <p>UID</p>
              </td>
              <td align="left">
                <p>
                  <span class="italic">String</span>
                </p>
              </td>
            </tr><tr valign="top">
              <td align="center">
                <p>UL</p>
              </td>
              <td align="left">
                <p>Unsigned Long</p>
              </td>
              <td align="left">
                <p>
                  <span class="italic">Number</span>
                </p>
              </td>
            </tr><tr valign="top">
              <td align="center">
                <p>UN</p>
              </td>
              <td align="left">
                <p>Unknown</p>
              </td>
              <td align="left">
                <p><span class="italic">Base64</span> encoded
        <span class="italic">string</span></p>
              </td>
            </tr><tr valign="top">
              <td align="center">
                <p>US</p>
              </td>
              <td align="left">
                <p>Unsigned Short</p>
              </td>
              <td align="left">
                <p>
                  <span class="italic">Number</span>
                </p>
              </td>
            </tr><tr valign="top">
              <td align="center">
                <p>UT</p>
              </td>
              <td align="left">
                <p>Unlimited Text</p>
              </td>
              <td align="left">
                <p>
                  <span class="italic">String</span>
                </p>
              </td>
            </tr></tbody></table>
           
## MSDICOM JSON Encoding
The `msdicom+json` media type is similar to the `dicom+json` media type, but
it uses JSON arrays instead of JSON objects for encoding attributes. 
`msdicom+json` is encoded as follows:

* A **Dataset** is represented as an array of attributes
* An **Attribute** is represented by an array containing three elements:
	1. **Tag** - an 8 character uppercase hexadecimal string
	2. **Value Representation** - a two letter code that is the same as 
	  `dicom+json, and
	3. **value** - zero or more JSON values, multiple values as stored as 
	  an array.  
* A **Sequence** Attribute has a value that is an array of zero or more
  **Datasets**.

MSDICOM is a general format that can encode a patient and zero or more studies
in a single object.  It can also encode any part or multiple parts of a study 
in a single object.  In order to do that is uses the following special
Sequences:

* **Patient-Studies Sequence**: A Sequence that contains:   
    1. A **File Meta Information** (FMI) dataset, that might be empty,
    2. A **Patient** dataset, and
    3. Zero or more Study datasets

* **Series Sequence**: 
A Sequence, contained within a Study dataset, that contains a dataset for each
Series in the study.  The Series Sequence must be an attribute at the top
level of a Study dataset.

*  **Instance Sequence**: A Sequence, contained within a Series Sequence that
contains a dataset for   each Instance contained in the containing Series.  
The Instance Sequence must be an attribute at the top level of a Series 
dataset.

Series are nested inside a Study and Instances are nested inside a Series.