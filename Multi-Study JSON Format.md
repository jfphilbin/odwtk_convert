Format of a Multi-Study Dataset in JSON

{patientStudySequence: [
  [FMI Dataset]
  [patient Dataset]
  [study 0 Dataset]
  [study 1 Dataset]
  ...
  [study N dataset] 
}

Where a Study Dataset is
[ [attribute]*
  [Series Sequence
    [Series 0 Dataset]
    [Series 1 Dataset]
    ...
    [Series N Dataset]
    ]
   []attribute]*
   ]
    
Where a Series Dataset is:

[ [attribute]*
  [Instance Sequence
    [Instance 0 Dataset]
    [Instance 1 Dataset]
    ...
    [Instance N Dataset]
    }
  [attribute]*
  ]